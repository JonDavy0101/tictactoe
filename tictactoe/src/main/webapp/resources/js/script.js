/* Global Variables */
var gameOver = false;
var playComputer = false;
var player1Turn = true;
var player1Symbol = "";
var player2Symbol = "";
var player1 = "Player 1";
var player2 = "Player 2";
var player1Wins = false;
var player2Wins = false;
var playerTie = false;

var arr = ["a1","a2","a3","b1","b2","b3","c1","c2","c3"];

var player1Arr = {
	"a1" : false,
	"a2" : false,
	"a3" : false,
	"b1" : false,
	"b2" : false,
	"b3" : false,
	"c1" : false,
	"c2" : false,
	"c3" : false
}

var player2Arr = {
	"a1" : false,
	"a2" : false,
	"a3" : false,
	"b1" : false,
	"b2" : false,
	"b3" : false,
	"c1" : false,
	"c2" : false,
	"c3" : false
}


// On Load Event Listener
window.addEventListener("load", function() {
	// Start the game
	document.getElementById("play-btn").addEventListener("click", initializeGame);

	// Submit names
	document.getElementById("submit-names").addEventListener("click", setPlayerNames);

	// Play game
	document.getElementById("game-board").addEventListener("click", function(e) {
		var id = e.target.id;
		setBoardValues(id);
	});

	// Reset game
	document.getElementById("reset-btn").addEventListener("click", resetGame);

	// Hide game board
	document.getElementById("game-board").style.display = "none";
});

function initializeGame() {
	
	if (confirm('Would you like to play a game?')) {
	    playComputer = true;
	    console.log("Play game");
	}

	// Hide Play button, display the Add Names form
	document.getElementById("play-btn").style.display = "none";
	document.getElementById("add-names-form").style.display = "block";
	document.getElementById("messages").innerHTML = "Enter your player names";
	
	// Get random player and assign X and O
	if (randomNumber(0,1) == 0) {
		player1Turn = true;
		player1Symbol = "x";
		player2Symbol = "o";
	} else {
		player1Turn = false;
		player1Symbol = "o";
		player2Symbol = "x";
	}
}

function checkForWinnerOrCat() {
	console.log("checkForWinnerOrCat");
	if ((player1Arr.a1 == true && player1Arr.a2 == true && player1Arr.a3 == true) || (player1Arr["b1"] == true && player1Arr["b2"] == true && player1Arr["b3"] == true) || (player1Arr["c1"] == true && player1Arr["c2"] == true && player1Arr["c3"] == true) || (player1Arr["a1"] == true && player1Arr["b1"] == true && player1Arr["c1"] == true) || (player1Arr["a2"] == true && player1Arr["b2"] == true && player1Arr["c2"] == true) || (player1Arr["a3"] == true && player1Arr["b3"] == true && player1Arr["c3"] == true) || (player1Arr["a1"] == true && player1Arr["b2"] == true && player1Arr["c3"] == true) || (player1Arr["a3"] == true && player1Arr["b2"] == true && player1Arr["c1"] == true)) {
		player1Wins = true;
		return true;
	} else if ((player2Arr["a1"] == true && player2Arr["a2"] == true && player2Arr["a3"] == true) || (player2Arr["b1"] == true && player2Arr["b2"] == true && player2Arr["b3"] == true) || (player2Arr["c1"] == true && player2Arr["c2"] == true && player2Arr["c3"] == true) || (player2Arr["a1"] == true && player2Arr["b1"] == true && player2Arr["c1"] == true) || (player2Arr["a2"] == true && player2Arr["b2"] == true && player2Arr["c2"] == true) || (player2Arr["a3"] == true && player2Arr["b3"] == true && player2Arr["c3"] == true) || (player2Arr["a1"] == true && player2Arr["b2"] == true && player2Arr["c3"] == true) || (player2Arr["a3"] == true && player2Arr["b2"] == true && player2Arr["c1"] == true)) {
		player2Wins = true;
		return true;
	} else if ((player1Arr.a1 == true || player2Arr.a1 == true) && (player1Arr.a2 == true || player2Arr.a2 == true) && (player1Arr.a3 == true || player2Arr.a3 == true) && (player1Arr.b1 == true || player2Arr.b1 == true) && (player1Arr.b2 == true || player2Arr.b2 == true) && (player1Arr.b3 == true || player2Arr.b3 == true) && (player1Arr.c1 == true || player2Arr.c1 == true) && (player1Arr.c2 == true || player2Arr.c2 == true) && (player1Arr.c3 == true || player2Arr.c3 == true)) {
		playerTie = true;
		return true;
	} else {
		return false;
	}
}

function setBoardValues(location) {
	var currentVal = document.getElementById(location).src;

	// Check for empty space, then set values
	if (clickEmptySpace(currentVal)) {
		document.getElementById(location).src = `./resources/img/${whichPlayer(location)}.png`;
		
		if (checkForWinnerOrCat() == true) {
			if (player1Wins == true) {
				alert(`${player1} wins!`);
			} else if (player2Wins == true) {
				alert(`${player2} wins!`);
			} else {
				alert("The game was a tie!");
			}
			document.getElementById("reset-btn").style.display = "inline";
		}
	}
}

function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min; 
}

function announceTurn() {			
	if (player1Turn == true) {
		document.getElementById("messages").innerHTML = `It is ${player1}'s turn`;
	} else {
		document.getElementById("messages").innerHTML = `It is ${player2}'s turn`;
	}
}

function whichPlayer(location) {
	console.log(`whichPlayer[${location}]`);
	if (player1Turn == true) {
		player1Arr[location] = true;
		console.log(`player1Arr[${location}]=${player1Arr[location]}`);
		player1Turn = false;
		announceTurn();
		return player1Symbol;
	} else {
		player2Arr[location] = true;
		console.log(`player2Arr[${location}]=${player2Arr[location]}`);
		player1Turn = true;
		announceTurn();
		return player2Symbol;
	}
}

function clickEmptySpace(location) {
	if (location.endsWith("blank.png")) {
		return true;
	} else {
		return false;
	}
}

function setPlayerNames() {
	player1 = document.getElementById("player-name").value;
	player2 = document.getElementById("computer-name").value;
	document.getElementById("players").innerHTML = `${player1} | ${player2}`;
	console.log(`${player1} | ${player2}`);
	document.getElementById("add-names-form").style.display = "none";
	document.getElementById("game-board").style.display = "block";
	document.getElementById("players").style.display = "block";
				
	// Announce Turn
	announceTurn();
}

function resetGame() {
    gameOver = false;
	playComputer = false;
	player1Turn = true;
	player1Symbol = "";
	player2Symbol = "";
	player1Wins = false;
	player2Wins = false;
	playerTie = false;
	document.getElementById("messages").innerHTML = "";
	document.getElementById("board-game").style.display = "none";
	document.getElementById("players").style.display = "none";

	for (let i = 0; i < arr.length; i++) {
		x = arr[i];

		// Reset Board
		document.getElementById(x).innerHTML = "";
		player1Arr[x] = false;
		player2Arr[x] = false;
	}

	initializeGame();
}